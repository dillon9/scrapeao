#The Scraper
#Dillon
#4/23/2014

import urllib2 as u
import xlsxwriter as xl
from bs4 import BeautifulSoup

def baseFormat (data):
    if type(data) == type(""):
        response = u.urlopen(data)
        return response.read()

siteList = [line.strip() for line in open("sites.txt","r")]
workbook = xl.Workbook("ScrapedData.xlsx")
worksheet = workbook.add_worksheet()
worksheet.write("A1","Title")
worksheet.write("B1","Links")
row,col = 0,0

for i in enumerate(siteList):
    html = baseFormat(siteList[i[0]])
    soup = BeautifulSoup(html)

    worksheet.write(row+1,col,soup.title.string)
    for link in soup.find_all("a"):
        qCL = link.get("href")
        if qCL == "#" or qCL == "/" or str(qCL)[:6] == "engine":
            continue
        worksheet.write(row+1,col+1,qCL)
        row += 1

workbook.close()
print "Process completed"
